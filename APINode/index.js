const express = require("express");
const fs = require("fs");
let bodyParser = require("body-parser");
const app = express();
let dataBase = null;
const response = {
    success: true,
    data: {},
    message: ""
}
const requestMessage = "";
const errorLoadData = false;

app.listen(3000, () => {
    console.log("El servidor está inicializado en el puerto 3000");
    fs.readFile('data.json', 
        function(err, data) {
            if (err) {
                console.log(err);
                errorLoadData = true;
                requestMessage = "Error consultando el archivo de datos data.json";
                dataBase = null;
            }
            if (data) {
                dataBase = JSON.parse(data);
            }
        }
    );
});

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});
  
app.get('/getAllHotels', function (req, res) {
    response['success'] = !errorLoadData;
    response['message'] = errorLoadData ? response['message'] : 'Consulta exitosa';
    response['data'] = dataBase === null ? {} : dataBase;
    res.send(response);
});

app.post('/postFilterHotels', function (req, res) {
    response['success'] = !errorLoadData;
    response['message'] = errorLoadData ? response['message'] : 'Consulta exitosa';
    if (dataBase !== null) {
        const tempData = [];
        dataBase.forEach(function (item) {
            if (req.body.typeFilter === 'name') {
                if (item.name.indexOf(req.body.filter) > -1) {
                    tempData.push(item);
                }
            } else if (req.body.typeFilter === 'stars') {
                if (item.stars === req.body.filter) {
                    tempData.push(item);
                }
            } else {
                response['message'] = 'El filtro usado no existe';
            }
        });
        response['data'] = tempData.length === 0 ? dataBase : tempData;
    } else {
        response['data'] = {};
    }
    res.send(response);
});